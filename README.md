# MyCargonaut
![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)

<sub><sup>Disclaimer: This project is part of the "Konzepte moderner Softwareentwicklung" module, led by Lukas Maximilian Kimpel. This web application, MyCargonaut, is a student initiative aimed at exploring and implementing modern software development concepts.</sub></sup>

Welcome to MyCargonaut, your go-to platform for facilitating ride and delivery services! Our web application is designed to seamlessly connect individuals with transportation needs to those who have available space and are willing to help.

## Features

### Feature 1
Text
### Feature 2
Text
### Feature 3
Text

## Installation
Text

## Contributors
All members of "Group 2" were involved in the project.

* Marcel Kaiser     - [Gitlab](https://git.thm.de/mpks28)   - [Github](https://github.com/marcel951)
* Jonathan Rech     - [Gitlab](https://git.thm.de/jwhr06)   - [Github](https://github.com/JonathanRech)
* Benjamin Wirth    - [Gitlab](https://git.thm.de/bwrt47)   - [Github](https://github.com/wrth1337)
* Ella Maria Herr   - [Gitlab](https://git.thm.de/emhr14)
* Nicolas Daniel Binder -  [Giblab](https://git.thm.de/ndbn17)
* Hanna Feddersen   -[Gitlab](https://git.thm.de/hfdd02)
