# Technology selection
##### **Frontend**: Angular
##### **Backend**: ExpressJS
##### **Database**: MariaDB
##### **Documentation**: Swagger
##### **Testing**: Jest as Testframework for Frontend and Backend
##### **CI/CD-Pipeline**:
* **Commit-Phase**: ESLint, Tests via Jest
* **Report-Phase**: Sonarqube
* **UAT-Phase**:
* **PAT-Phase**: 
* **SAT-Phase**: Dependabot
* **Deploy-Phase**: Building a Docker-Container from master branch

# Designs
### Wireframe
[- Insert Picture/URL here -]
### Mockup
[- Insert Picture/URL here -]
